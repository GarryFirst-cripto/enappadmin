import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { Header as HeaderUI, Image, Grid, Icon, Button } from 'semantic-ui-react';
import styles from './styles.module.scss';

const Header = ({ user, logout, editUser }) => {
  function doLogout() {
    logout();
  }
  return (
    <div className={styles.headerWrp}>
      <Grid centered container columns="5">
        <Grid.Column>
          <NavLink exact to="/" onClick={() => editUser(null)}>
            <HeaderUI>
              <Image
                style={{ height: '36px', width: '36px' }}
                src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS-__PcIPIkhSEoTE0MGKCh1t9Y8_mT4h5NAwgIpnAlnqenwTQi2sze8sVNGIkSIgYQY7s&usqp=CAU"
              />
              {' '}
              U s e r s
              {' '}
            </HeaderUI>
          </NavLink>
        </Grid.Column>
        <Grid.Column>
          <NavLink exact to="/messanger" >
            <HeaderUI>
              <Image style={{ height: '36px', width: '36px' }} src="https://tell-different.com/images/news/2015/04/feedback_cloud.png" />
              {' '}
              Messenger
              {' '}
            </HeaderUI>
          </NavLink>
        </Grid.Column>
        <Grid.Column>
          <NavLink exact to="/words" >
            <HeaderUI>
              <Image style={{ height: '36px', width: '36px' }} src="https://stylus.ua/thumbs/568x568/0e/7a/997211.jpeg" />
              {' '}
              Dictionary
              {' '}
            </HeaderUI>
          </NavLink>
        </Grid.Column>
        <Grid.Column>
          <NavLink exact to="/updater" >
            <HeaderUI>
              <Image style={{ height: '36px', width: '36px' }} src="https://almamater13.ru/wp-content/uploads/mehanik-eto-dolzhnost-ili-professiya_2.jpg" />
              {' '}
              Updater
              {' '}
            </HeaderUI>
          </NavLink>
        </Grid.Column>
        <Grid.Column textAlign="right">
          <Button basic icon type="button" className={`${styles.menuBtn} ${styles.logoutBtn}`} onClick={doLogout}>
            <Icon name="log out" size="large" />
            {' '}
            Log Out 
          </Button>
        </Grid.Column>
      </Grid>
    </div>
  );
};

Header.propTypes = {
  logout: PropTypes.func.isRequired,
  user: PropTypes.objectOf(PropTypes.any).isRequired
};

export default Header;
