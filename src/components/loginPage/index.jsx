import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { login } from 'src/services/actions';
import Logo from 'src/components/logo';
import { Grid, Header } from 'semantic-ui-react';
import LoginForm from './loginForm';

const LoginPage = ({ login: signIn }) => (
  <Grid textAlign="center" verticalAlign="middle">
    <Grid.Column style={{ width: '450px', paddingTop: '100px' }}>
      <Logo />
      <Header as="h2" color="teal" textAlign="center">
        Login to Admin account
      </Header>
      <LoginForm login={signIn} />
    </Grid.Column>
  </Grid>
);

LoginPage.propTypes = {
  login: PropTypes.func.isRequired
};

const actions = { login };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  null,
  mapDispatchToProps
)(LoginPage);