import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Segment } from 'semantic-ui-react';

const LoginForm = ({ login }) => {
  const [pwd, setPassword] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isPasswordValid, setIsPasswordValid] = useState(true);

  const passwordChanged = data => {
    setPassword(data);
    setIsPasswordValid(true);
  };

  const dataValid = () => {
    setIsPasswordValid(Boolean(pwd));
  };

  const handleLoginClick = async () => {
    if (!isPasswordValid || isLoading) {
      alert('You must enter the correct data !');
      return;
    }
    setIsLoading(true);
    try {
      const user = await login({ pwd });
      setIsLoading(false);
      if (!user) {
        alert('Access denied ! Invalid password.');
      }
    } catch {
      setIsLoading(false);
    }
  };

  return (
    <Form name="loginForm" size="large" onSubmit={handleLoginClick} >
      <Segment>
        <Form.Input
          fluid
          icon="lock"
          iconPosition="left"
          placeholder="Password"
          type="password"
          error={!isPasswordValid}
          onChange={ev => passwordChanged(ev.target.value)}
          onBlur={() => setIsPasswordValid(Boolean(pwd))}
        />
        <Button type="submit" color="teal" fluid size="large" loading={isLoading} primary onClick={dataValid}>
          Login
        </Button>
      </Segment>
    </Form>
  );
};

LoginForm.propTypes = {
  login: PropTypes.func.isRequired
};

export default LoginForm;
