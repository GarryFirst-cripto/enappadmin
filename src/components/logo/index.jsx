import React from 'react';
import { Image, Header } from 'semantic-ui-react';

export const Logo = () => (
  <Header as="h2" color="grey" >
    <Image src="https://almamater13.ru/wp-content/uploads/mehanik-eto-dolzhnost-ili-professiya_2.jpg" style={{ height: 150, width: 180 }}/>
    {' '}
    Enapp
  </Header>
);

export default Logo;
