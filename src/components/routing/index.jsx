import React, { useEffect } from 'react';
import { Route, Switch } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Header from '../header';
import LoginPage from '../loginPage';
import Users from 'src/scenes/users';
import Dictionary from 'src/scenes/dictionary';
import Messages from 'src/scenes/messanger';
import Updater from 'src/scenes/updater';
import PrivateRoute from '../privatRoute';
import PublicRoute from '../publicRoute';
import NotFound from '../notFound';
import Spinner from '../spinner';
import { doAdminLogin, doAdminEntrance, logout, editUser } from 'src/services/actions';
import PropTypes from 'prop-types';

const Routing = ({
  user,
  isAuthorized,
  logout: signOut,
  editUser,
  doAdminLogin: adminLogin, 
  doAdminEntrance: adminEntrance,
  isLoading
}) => {
  useEffect(() => {
    if (!isAuthorized) {
      const token = localStorage.getItem('token');
      if (token) {
        adminLogin(token);
      } else {
        adminEntrance();
      }
    }
  });

  return (
    isLoading
      ? <Spinner />
      : (
        <div className="fill">
          {isAuthorized && (
            <header>
              <Header user={user} logout={signOut} editUser={editUser} />
            </header>
          )}
          <main>
            <Switch>
              <PublicRoute exact path="/login" component={LoginPage} />
              <PrivateRoute exact path="/" component={Users} />
              <PrivateRoute exact path="/messanger" component={Messages} />
              <PrivateRoute exact path="/words" component={Dictionary} />
              <PrivateRoute exact path="/updater" component={Updater} />
              <Route path="*" exact component={NotFound} />
            </Switch>
          </main>
        </div>
      )
  );
};

Routing.propTypes = {
  isAuthorized: PropTypes.bool,
  logout: PropTypes.func.isRequired,
  doAdminLogin: PropTypes.func.isRequired,
  doAdminEntrance: PropTypes.func.isRequired,
  user: PropTypes.objectOf(PropTypes.any),
  isLoading: PropTypes.bool
};

Routing.defaultProps = {
  isAuthorized: false,
  user: {},
  isLoading: true
};

const actions = { doAdminLogin, doAdminEntrance, logout, editUser };

const mapStateToProps = ({ profile }) => ({
  isAuthorized: profile.isAuthorized,
  user: profile.user,
  isLoading: profile.isLoading
});

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Routing);
