const female = 'https://thumbs.dreamstime.com/b/young-woman-avatar-cartoon-character-profile-picture-young-brunette-woman-short-hair-avatar-cartoon-character-vector-149656725.jpg';
const male = 'https://media.istockphoto.com/vectors/man-face-cartoon-vector-id878942172?k=6&m=878942172&s=170667a&w=0&h=t9tem3ukRdCO7sE9FCyW5D8sgugW5elzib-_Dkk3pEU=';
const defmale = 'https://forwardsummit.ca/wp-content/uploads/2019/01/avatar-default.png';

const userImage = userSex => {
  switch (userSex) {
    case 'male':
      return male;
    case 'female':
      return female;
    default:
      return defmale;
  }
};

export default userImage;
