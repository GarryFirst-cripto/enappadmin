const userAge = date => {
  if (date) {
    const now = new Date();
    const today = new Date(now.getFullYear(), now.getMonth(), now.getDate());
    const dob = new Date(date);
    const dobnow = new Date(today.getFullYear(), dob.getMonth(), dob.getDate());
    const koe = today < dobnow ? 1 : 0;
    return today.getFullYear() - dob.getFullYear() - koe;
  };
  return 'unknown...';
};

export default userAge;
