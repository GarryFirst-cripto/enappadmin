import React from 'react';
import { render } from 'react-dom';
import Home from './components/home';

import 'semantic-ui-css/semantic.min.css';
import './styles/reset.scss';
import './styles/common.scss';

render(<Home />, document.getElementById('root'));
