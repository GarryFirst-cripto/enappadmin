import React from 'react';
import { Header, Image } from 'semantic-ui-react';

const Dictionary = () => {

  return (
    <div className="login-form">
      <Header as="h2" className="login-logo">
        <Image src="https://almamater13.ru/wp-content/uploads/mehanik-eto-dolzhnost-ili-professiya_2.jpg" style={{ height: 150, width: 180 }} />
        {' '}
        Dictionary
      </Header>
    </div>
  );
};

export default Dictionary;
