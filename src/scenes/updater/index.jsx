import React, { useState } from 'react';
import { Header, Message, Image, Button } from 'semantic-ui-react';
import ReactFileReader from 'react-file-reader';
import XLSX from 'xlsx';
import { writeWordsAction } from 'src/services/wordsService';
import './styles.css';

const Rls = String.fromCharCode(10);
const Fls = String.fromCharCode(9);

const parseData = data => {
  const list = Object.keys(data).map((item, index) => {
    return (
      <div key={index} className="user-item">
        {`${item} --> ${data[item]}`}
      </div>
    );
  });
  return list;
};

const wordList = [];
const numbFields = ['WordEnFrq', 'MeaningProc'];
const comFields = ['RuComment', 'RuUsage'];
const headersStd = [
  { old: 'En', new: 'WordEn' },
  { old: 'EnFrq', new: 'WordEnFrq' },
  { old: 'Ru', new: 'WordRu' },
  { old: 'Ru%', new: 'MeaningProc' },
  { old: 'Hint', new: 'Hint1' },
  { old: '"ID"', new: 'ID' },
  { old: 'Meaning%', new: 'MeaningProc' }
];

const Updater = () => {

  const [isWriting, setWriting] = useState(false);
  const [data, setData] = useState({});

  const readFile = file => {
    return new Promise((resolve, reject) => {
      let reader = new FileReader();
      reader.onload = () => {
        resolve(reader.result);
      };
      reader.onerror = reject;
      reader.readAsArrayBuffer(file);
    });
  };

  const replacer = text => {
    for (let i = 0; i < headersStd.length; i++) {
      if (headersStd[i].old === text) {
        return headersStd[i].new;
      }
    }
    return text;
  };

  const doFileLoading = async files => {
    const file = files[0];
    const buff = await readFile(file);
    const wBook = XLSX.read(buff, { type: 'array' });
    const wSheet = wBook.Sheets[wBook.SheetNames[0]];
    const data = XLSX.utils.sheet_to_csv(wSheet, { header: 1, RS: Rls, FS: Fls }).split(Rls);
    // const headers = data[0].split(Fls).map(item => item.replace('%', 'Proc').replace('Ru', 'WordRu'));
    const headers = data[0].split(Fls).map(item => replacer(item));
    wordList.length = 0;
    for (let i = 1; i < data.length - 1; i++) {
      const values = data[i].split(Fls);
      const ress = {};
      headers.forEach((item, index) => { ress[item] = values[index]; });
      numbFields.forEach(field => {
        ress[field] = ress[field].replace(' ', '');
      });
      comFields.forEach(field => {
        if (ress[field] === '0') ress[field] = '';
      });
      wordList.push(ress);
    };
    setData({ mess: `Подготовлено для записи ${wordList.length} строк.` });
  };

  const doWriteWords = async () => {
    if (wordList.length <= 0) {
      alert("Нечего записывать : список пока пуст ...");
      return;
    }
    setWriting(true);
    const result = await writeWordsAction(wordList);
    const wrData = [{ mess: 'Записано новых слов : ', indd: 0 }, { mess: 'Уже записаны в словарь : ', indd: 0 }];
    result.forEach((item, index) => {
      if (item.id) {
        wrData[0].indd++;
      } else if (item.status === 200 && item.message === 'Ok') {
        wrData[0].indd++;
      } else if (item.status === 200 && item.message === 'Already exists') {
        wrData[1].indd++;
      } else {
        wrData.push({ mess: 'Ошибка в строке -> ', indd: index + 1 });
      }
    });
    const newData = wrData.map(item => (`${item.mess} ${item.indd}`));
    setData(newData);
    setWriting(false);
  };

  return (
    <div className="login-form">
      <Header as="h2" className="login-logo">
        <Image src="https://almamater13.ru/wp-content/uploads/mehanik-eto-dolzhnost-ili-professiya_2.jpg" style={{ height: 150, width: 180 }} />
        {' '}
        Updates our dictionary
      </Header>
      <Message className="login-list">
        <ReactFileReader download fileTypes={[".xlsx"]} multipleFiles handleFiles={doFileLoading} >
          <Button style={{ width: 250 }} type="submit" color="teal" fluid size="large">
            Load XLSX file
          </Button>
        </ReactFileReader>
        <Button style={{ width: 250 }} type="submit" color="teal" fluid size="large" loading={isWriting} onClick={doWriteWords}>
          Write words
        </Button>
      </Message>
      {parseData(data)}
    </div>
  );
};

export default Updater;
