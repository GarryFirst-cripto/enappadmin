import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Header as HeaderUI, Image, Grid, Icon } from 'semantic-ui-react';
import { setUserPage, editUser } from 'src/services/actions';
import styles from '../styles.module.scss';

const Selector = ({ setUserPage, editUser }) => {
  const doSelect = value => {
    setUserPage(value);
  };
  const doCancel = () => {
    editUser(null);
  };

  return (
    <div className={styles.headerWrp}>
      <Grid centered container columns="5">
        <Grid.Column onClick={() => doSelect(0)} style={{ cursor: 'pointer', width: '190px' }}>
          <HeaderUI>
            <Image style={{ height: '36px', width: '36px' }} src="https://image.flaticon.com/icons/png/512/190/190766.png" />
            {' '}
            Statistic
            {' '}
          </HeaderUI>
        </Grid.Column>
        <Grid.Column onClick={() => doSelect(1)} style={{ cursor: 'pointer', width: '190px' }}>
          <HeaderUI>
            <Image
              style={{ height: '36px', width: '36px' }}
              src="https://thumbs.dreamstime.com/z/%D1%81-%D0%BE%D0%B2%D0%B0%D1%80%D1%8C-81050593.jpg" />
            {' '}
            Vocabulary
            {' '}
          </HeaderUI>
        </Grid.Column>
        <Grid.Column onClick={() => doSelect(2)} style={{ cursor: 'pointer', width: '190px' }}>
          <HeaderUI>
            <Image style={{ height: '36px', width: '36px' }} src="https://falconediting.com/img/service-1.png" />
            {' '}
            User Editor
            {' '}
          </HeaderUI>
        </Grid.Column>
        <Grid.Column onClick={doCancel} className={`${styles.menuBtn} ${styles.logoutBtn}`}>
          <HeaderUI >
            <Icon name="cancel" size="small" color='grey' />
            Exit
            {' '}
          </HeaderUI>
        </Grid.Column>
      </Grid>
    </div>
  );
};

const actions = { setUserPage, editUser };
const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(null, mapDispatchToProps)(Selector);
