import React, { useRef } from 'react';
import { Label, Icon, Image } from 'semantic-ui-react';
import moment from 'moment';
import userImage from '../../../helpers/imageHelper';
import userAge from '../../../helpers/userAgeHelper';
import styles from '../styles.module.scss';

const UserCard = ({ user, filter, editUser, setStatLoading, reference }) => {
  const { email, sex, age, status, sensay, createdAt } = user;

  const doEditUser = () => {
    setStatLoading();
    editUser(user);
  };

  const getUserMail = () => {
    if (filter && filter.length > 0) {
      const i = email.toLowerCase().indexOf(filter.toLowerCase());
      const j = filter.length;
      return (
        <span className={styles.nameSpan}>
          {' '}
          {email.substr(0, i)}
          <span style={{ backgroundColor: '#8e8' }}>{email.substr(i, j)}</span>
          {email.substr(i + j)}
        </span>
      );
    };
    return (
      <span className={styles.nameSpan}>
        {' '}
        {email}
      </span>
    );
  };

  const titleRef = useRef();
  const date = moment(createdAt).fromNow();

  return (
    <div ref={titleRef} className={styles.usercard} onClick={doEditUser}>
      {reference(titleRef)}
      <div style={{ display: 'flex', alignItems: 'center' }}>
        <Image className={styles.postImage} src={userImage(sex)} wrapped />
        {getUserMail()}
        <span className={styles.textSpan}>  
          {' '}
          {' age : '}
          {userAge(age)}
        </span>
        <span className={styles.textSpanRegg}>
          registered
          {' - '}
          {date}            
        </span> 
        <span className={styles.textSpanBlock}>
          {status && (
            <span>
              status
              {' - '}
              {status}
            </span>
          )}
        </span>   
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={doEditUser}>
          <Icon name="edit" />
          I n f o
        </Label>                 
      </div>
      {sensay && (
        <div className={styles.textSpanSensay}>
          sensay :
          {' '}
          {sensay}
        </div>
      )}
    </div>
  );
};

export default UserCard;
