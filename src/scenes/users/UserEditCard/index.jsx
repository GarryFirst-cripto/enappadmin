import React, { useState } from 'react';
import { Grid, Input, Dropdown, TextArea, Button, Icon, Image } from 'semantic-ui-react';
import validator from 'validator';
import userImage from '../../../helpers/imageHelper';
import styles from '../styles.module.scss';

const userSex = [
  {
    key: 'Male',
    text: 'Male',
    value: 'male',
    image: { avatar: true, src: userImage('male') }
  },
  {
    key: 'Female',
    text: 'Female',
    value: 'female',
    image: { avatar: true, src: userImage('female') }
  }
];

const UserEditCard = ({ user, editUser, updateUser, deleteUser }) => {

  const [sex, setUserSex] = useState(['male','female'].includes(user.sex) ? user.sex : null);
  const [email, setEmail] = useState(user.email);
  const [password, setPassword] = useState(user.password);
  const [status, setStatus] = useState(user.status);
  const [age, setUserAge] = useState(user.age);
  const [sensay, setSensay] = useState(user.sensay);
  const [viewType, setViewType] = useState({ viewMode: 'password', iconName: 'unhide' });
  const [edited, setEdited] = useState({ dataedit: false, imgedit: false });
  const [isLoading, setLoading] = useState(false);
  const [isEmailValid, setEmailValid] = useState(true);
  const [isPasswordValid, setPasswordValid] = useState(true);

  const changeView = () => {
    if (viewType.viewMode === 'password') {
      setViewType({ viewMode: 'text', iconName: 'hide' });
    } else {
      setViewType({ viewMode: 'password', iconName: 'unhide' });
    }
  };

  async function handleUpdateProfile() {
    if (edited) {
      const isValid = isEmailValid && isPasswordValid;
      if (!isValid || isLoading) {
        if (!isValid) alert('You must enter the correct data !');
        return;
      }
      setLoading(true);
      try {
        const { id } = user;
        await updateUser({ id, email, password, sex, age, status, sensay });
        setLoading(false);
        editUser(null);
      } catch {
        setLoading(false);
      }
    };
    editUser(null);
  }

  async function handleCancel() {
    await editUser(null);
    setLoading(false);
  }

  async function handleDelete() {
    // eslint-disable-next-line no-restricted-globals
    if (confirm(`Delete user ${email}`)) {
      deleteUser(user.id);
      editUser(null);
    }
  }

  return (
    <Grid container textAlign="center" className={styles.userEditCard}>
      <Grid.Column>
        <Image centered src={userImage(sex)} size="medium" circular />
        <br />
        <Input
          className={styles.profInput}
          icon="calendar alternate outline"
          iconPosition="left"
          placeholder="Date of Birth"
          type="date"
          value={age}
          onChange={event => {
            setEdited({ dataedit: true });
            setUserAge(event.target.value);
          }}
        />
        <br />
        <br />
        <Input
          className={styles.profInput}
          icon="envelope outline"
          iconPosition="left"
          placeholder="E-mail"
          type="email"
          value={email}
          error={!isEmailValid}
          onChange={event => {
            setEdited({ dataedit: true });
            setEmail(event.target.value);
          }}
          onBlur={() => setEmailValid((email) && validator.isEmail(email))}
        />
        <Input
          className={styles.profPass}
          icon="privacy"
          iconPosition="left"
          placeholder="Set new password"
          type={viewType.viewMode}
          value={password}
          error={!isPasswordValid}
          onChange={event => {
            setEdited({ dataedit: true });
            setPassword(event.target.value);
          }}
          onBlur={() => setPasswordValid((!password) || validator.isLength(password, { min: 5 }))}
        />
        <Button className={styles.profButton} onClick={changeView}>
          <Icon name={viewType.iconName} />
        </Button>
        <br />
        <br />
        <Input
          style={{ width: '20vw', marginLeft: 'calc(50% - 20vw' }}
          icon="flag checkered"
          iconPosition="left"
          placeholder="Status"
          type="text"
          value={status}
          onChange={event => {
            setEdited({ dataedit: true });
            setStatus(event.target.value);
          }}
        />
        <Dropdown
          style={{ width: '20vw', float: 'right', marginRight: 'calc(50% - 20vw' }}
          placeholder='Choose your gender'
          fluid
          selection
          options={userSex}
          value={sex}
          onChange={(event, { value }) => {
            // alert(event.target.textContent);
            // alert(value);
            setEdited({ dataedit: true });
            setUserSex(value);
          }}
        />        
        <br />
        <br />
        <TextArea
          className={styles.profText}
          rows="3"
          name="body"
          value={sensay}
          placeholder="Sensay text"
          onChange={event => {
            setEdited({ bodyedit: true });
            setSensay(event.target.value);
          }}
        />
        <br />
        <br />
        <Button color="red" onClick={handleDelete} loading={isLoading} style={{ width: '10vw', minWidth: '120px', marginRight: '2vw' }}>
          <Icon name="user cancel" />
          Delete User
        </Button>
        <Button color="green" onClick={handleUpdateProfile} loading={isLoading} style={{ width: '10vw', minWidth: '120px' }}>
          <Icon name="save" />
          Save profile
        </Button>
        <Button color="orange" onClick={handleCancel} loading={isLoading} style={{ width: '10vw', minWidth: '120px' }}>
          <Icon name="cancel" />
          Cancel
        </Button>
      </Grid.Column>
    </Grid>
  );
};

export default UserEditCard;
