import React, { useEffect } from 'react';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Selector from '../Selector';
import UserEditCard from '../UserEditCard';
import UserStatistic from '../UserSatistic';
import UserVocabulary from '../UserVocabulary';
import Spinner from '../../../components/spinner';
import { getStatistic, setVocReference, setExtWord } from 'src/services/actions';

const UserInfoCard = ({ user, stat, wordStat, editUser, updateUser, deleteUser, isStatLoading, isPage, getStatistic, setVocReference, filter, extWord, setExtWord }) => {
  useEffect(() => {
    if (isStatLoading) {
      getStatistic();
    }
  });

  return (
    isStatLoading
      ? <Spinner />
      : (
        <div className="fill">
          <main>
            {isPage === 0 && <UserStatistic stat={stat} wordStat={wordStat} />}
            {isPage === 1 && <UserVocabulary wordStat={wordStat} reference={setVocReference} filter={filter} extWord={extWord} setExtWord={setExtWord} />}
            {isPage === 2 && <UserEditCard user={user} editUser={editUser} updateUser={updateUser} deleteUser={deleteUser}/>}
          </main>
          <header>
            <Selector />
          </header>          
        </div>
      )
  );
};

UserInfoCard.propTypes = {
  isStatLoading: PropTypes.bool,
  isPage: PropTypes.number,
  stat: PropTypes.arrayOf(PropTypes.object),
  wordStat: PropTypes.arrayOf(PropTypes.object),
  filter: PropTypes.string,
  extWord: PropTypes.object,
  setVocReference: PropTypes.func.isRequired,
  getStatistic: PropTypes.func.isRequired,
  setExtWord: PropTypes.func.isRequired
};

UserInfoCard.defaultProps = {
  isStatLoading: true,
  isPage: 0,
  stat: [],
  wordStat: [],
  filter: '',
  extWord: undefined
};

const actions = { getStatistic, setVocReference, setExtWord };

const mapStateToProps = ({ profile }) => ({
  isStatLoading: profile.isStatLoading,
  isPage: profile.isPage,
  stat: profile.stat,
  wordStat: profile.wordStat,
  filter: profile.filter,
  extWord: profile.extWord
});

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserInfoCard);