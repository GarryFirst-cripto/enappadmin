import * as React from "react";
import { Chart } from 'react-google-charts';

const wordIndex = waight => {
  const waights = [50000, 20000, 5000, 1000];
  for (let i = 0; i < waights.length; i++) {
    if (waight > waights[i]) return i;
  }
  return 4;
};

const UserStatistic = ({ stat, wordStat }) => {
  
  const data = [[ '', 'Learned words', 'Learned total', 'Learned errors', 'Words in vocabulary' ]];
  const expp = [['', 'Experience']];
  const intens = [['', 'Intensity']];
  stat.forEach(item => {
    const dats = new Date(item.data).toLocaleString('ru-Ru', {timeZone: "Europe/Moscow"}).substr(0, 5);
    data.push([dats, item.learned, item.count, item.count - item.countTrue, item.allWords]);
    expp.push([dats, item.experience]);
    intens.push([dats, Math.round((item.countTrue / 55) * 100)]);
  });

  const wordDate = [
    ['Word`s groups', 'Words count', 'Bronse', 'Silver', 'Gold', 'Attempts', 'Errors'],
    ['Very light', 0, 0, 0, 0, 0, 0],
    ['Easy', 0, 0, 0, 0, 0, 0],
    ['Normal', 0, 0, 0, 0, 0, 0],
    ['Complex ', 0, 0, 0, 0, 0, 0],
    ['Very difficult', 0, 0, 0, 0, 0, 0]
  ];
  wordStat.forEach(item => {
    const index = wordIndex(item.waight);
    wordDate[index + 1][1] = wordDate[index + 1][1] + 1;
    wordDate[index + 1][5] = wordDate[index + 1][5] + item.count;
    wordDate[index + 1][6] = wordDate[index + 1][6] + item.count - item.countTrue;
    if (item.state >= 1 && item.state <= 5) wordDate[index + 1][2] = wordDate[index + 1][2] + 1;
    if (item.state >= 6 && item.state <= 10) wordDate[index + 1][3] = wordDate[index + 1][3] + 1;
    if (item.state >= 11) wordDate[index + 1][4] = wordDate[index + 1][4] + 1;
  });

  return (
    <div style={{ overflowY: 'auto', overflowX: 'hidden', height: 'calc(100vh - 145px)' }}>
      <Chart
        width={'100%'}
        height={'400px'}
        style={{ paddingTop: '-100px', paddingBottom: '-50px' }}
        chartType="LineChart"
        loader={<div>Loading Chart</div>}
        data={data}
        options={{
          hAxis: {
            title: 'Date'
          },
          vAxis: {
            title: 'Values'
          },
          series: {
            0: { color: 'lime', waight: 800 },
            1: { color: '#55f' },
            2: { color: 'red' },
            3: { color: 'green' }
          }
        }}
        rootProps={{ 'data-testid': '1' }}
      />
      <Chart
        width={'100%'}
        height={'300px'}
        style={{ paddingTop: '-50px', paddingBottom: '-50px' }}
        chartType="LineChart"
        loader={<div>Loading Chart</div>}
        data={expp}
        options={{
          hAxis: {
            title: 'Date'
          },
          vAxis: {
            title: 'Experience'
          }
        }}
        rootProps={{ 'data-testid': '1' }}
      />
      <Chart
        width={'100%'}
        height={'300px'}
        style={{ paddingTop: '-50px', paddingBottom: '-50px' }}
        chartType="LineChart"
        loader={<div>Loading Chart</div>}
        data={intens}
        options={{
          hAxis: {
            title: 'Date'
          },
          vAxis: {
            title: 'Intensity'
          },
          series: {
            0: { color: 'green' }
          }
        }}
        rootProps={{ 'data-testid': '1' }}
      />
      <Chart
        width={'100%'}
        height={'250px'}
        chartType="Bar"
        loader={<div>Loading Chart</div>}
        data={wordDate}
        options={{
          chart: {
            title: 'User Vocabulary'
          },
          series: {
            0: { color: 'lime' },
            1: { color: '#ddd' },
            2: { color: '#baa' },
            3: { color: 'orange' },
            4: { color: '#0df' },
            5: { color: 'red' }
          }
        }}
        rootProps={{ 'data-testid': '2' }}
      />
    </div>
  );
};

export default UserStatistic;
