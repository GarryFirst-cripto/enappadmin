import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Spinner from 'src/components/spinner';
import LoggCard from './loggCard';
import styles from '../styles.module.scss';

const ExtWordCard = ({ word, loggs, isLoggLoading }) => {

  // const logs = stat.filter(item => (item.) )

  return (
    <div className={styles.usercard}>
      {isLoggLoading
        ? (<Spinner />)
        : (<LoggCard loggs={loggs} word={word} />)}          
    </div>
  );
};

ExtWordCard.propTypes = {
  loggs: PropTypes.arrayOf(PropTypes.object),
  isLoggLoading: PropTypes.bool
};

ExtWordCard.defaultProps = {
  loggs: [],
  isLoggLoading: false
};

const mapStateToProps = ({ profile }) => ({
  isLoggLoading: profile.isLoggLoading,
  loggs: profile.loggs
});

export default connect(mapStateToProps)(ExtWordCard);
