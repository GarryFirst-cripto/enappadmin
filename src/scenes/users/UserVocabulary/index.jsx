import * as React from "react";
import WordTitle from './wordTitle';
import WordCard from './wordCard';
import ExtWordCard from './extWordcard';

const UserStatistic = ({ wordStat, reference, filter, extWord, setExtWord }) => {
  const height = extWord ? 'calc(40vh - 155px)' : 'calc(100vh - 155px)';
  
  return (
    <div style={{ overflow: 'hidden', height: 'calc(100vh - 155px)' }}>
      <div style={{ overflowY: 'auto', overflowX: 'hidden', height: height, backgroundColor: '#eee' }}>
        <WordTitle />
        {wordStat.map((word, index) => (
          word.hidden ? null : <WordCard key={index} word={word} filter={filter} reference={reference} setExtWord={setExtWord} />
        ))}
      </div>
      {extWord && (
        <div style={{ marginTop: '5px', marginBottom: '3px', overflowY: 'auto', overflowX: 'hidden', height: '60vh', backgroundColor: '#ddd' }}>
          <ExtWordCard word={extWord} />
        </div>
      )}
    </div>
  );
};

export default UserStatistic;
