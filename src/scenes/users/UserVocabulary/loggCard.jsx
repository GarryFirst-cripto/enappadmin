import React from 'react';
import DataTable from 'react-data-table-component';

const LoggCard = ({ loggs, word }) => {

  const getLastPlane = (logg, index) => {
    for (let i = index - 1; i >= 0; i-- ) {
      const item = loggs[i];
      if (logg.userId === item.userId && item.index === logg.index - 1) {
        if (item.plane) return item.plane;
        return item.createdAt;
      }
    }
    return new Date();
  };

  const getDelay = value => {
    const days = Math.trunc(value / (24*3600*1000));
    const valH = value % (24*3600*1000);
    const hour = Math.trunc(valH / (3600*1000));
    const valM = valH % (3600*1000);
    const minn = Math.round(valM / (60*1000));
    const result = (days > 0 ? `${days} дн. ` : '') + (hour > 0 ? `${hour} ч. ` : '') + (minn > 0 ? `${minn} мин. ` : '');
    return result;
  };

  const getThought = value => {
    const hour = Math.trunc(value / (3600*1000));
    const valM = value % (3600*1000);
    const minn = Math.round(valM / (60*1000));
    const valS = valM % (60 * 1000);
    const secc = Math.round(valS / 1000);
    const result = (hour > 0 ? `${hour} ч. ` : '') + (minn > 0 ? `${minn} мин. ` : '') + (secc > 0 ? `${secc} сек.` : '');
    return result;
  };

  const data = [];
  loggs.forEach((item, index) => {
    if (item.mode === true && item.index > 0) {
      const { user: { email }, createdAt, answer, state, plane, experience, timing } = item;
      const last = getLastPlane(item, index);
      const diff = new Date(createdAt) - new Date(last);
      const delay = diff > 0 ? getDelay(diff) : '';
      const iThought = getThought(timing);
      // const lastPlane = (new Date(last)).toLocaleString("ru-RU", {timeZone: "Europe/Moscow"});
      const newDiff = new Date(plane) - new Date(createdAt);
      const period = newDiff > 0 ? getDelay(newDiff) : '';
      const iPlane = (new Date(plane)).toLocaleString("ru-RU", {timeZone: "Europe/Moscow"});
      const fact = (new Date(createdAt)).toLocaleString("ru-RU", {timeZone: "Europe/Moscow"});
      const iAnswer = answer ? 'Правильно' : 'Не правильно';
      data.push({ email, fact, delay, iThought, iAnswer, state, iPlane, period, experience });
    }
  });

  const customStyles = {
    title: {
      style: {
        backgroundColor: '#afa',
        cursor: 'default'
      }
    },
    rows: {
      style: {
        minHeight: '72px'
      }
    },
    headCells: {
      style: {
        backgroundColor: '#bfb',
        paddingLeft: '3px',
        paddingRight: '3px',
        cursor: 'default',
        fontSize: '16'
      }
    },
    cells: {
      style: {
        backgroundColor: '#bdb', 
        height: '30px',
        paddingLeft: '3px',
        paddingRight: '3px',
        cursor: 'default'
      }
    }
  };

  const columns = [
    { name: 'User', selector: 'email', sortable: true },
    { name: 'Fact time', selector: 'fact', sortable: true },
    { name: 'Delay', selector: 'delay', sortable: true },
    { name: 'Answer', selector: 'iAnswer', sortable: true },
    { name: 'Thought', selector: 'iThought', sortable: true, center: true },
    { name: 'Card State', selector: 'state', sortable: true, center: true },
    { name: 'Plane time', selector: 'iPlane', sortable: true },
    { name: 'Period', selector: 'period', sortable: true },
    { name: 'Experience', selector: 'experience', sortable: true, center: true }
  ];

  return (
    <DataTable
      title={`${word.WordEn} - ${word.WordRu}`}
      theme="solarized"
      columns={columns}
      data={data}
      dense
      highlightOnHover
      customStyles={customStyles}
    />
  );
};

export default LoggCard;