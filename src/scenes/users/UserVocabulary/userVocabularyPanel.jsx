import React, { useState } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Input, Button, Checkbox } from 'semantic-ui-react';
import { setVocFilter, getVocReffs, toggleShowLoggs } from 'src/services/actions';
// import styles from './styles.module.scss';

const VocabularyPanel = ({ wordStat, setVocFilter, getVocReffs, showLoggs, toggleShowLoggs }) => {
  
  const [filter, setFilter] = useState('');
  const [scroller, setScroller] = useState('');
  const [timer, setTimer] = useState();
  const [lastElem, setLastElem] = useState();
  const [lastColor, setLastColor] = useState();
  let filtText;

  const handleFilter = () => {
    clearTimeout(timer);
    setVocFilter(filtText);
  };

  const handleCancel = () => {
    clearTimeout(timer);
    setFilter('');
    setVocFilter('');
  };

  const doSetFilter = value => {
    setFilter(value);
    filtText = value;
    clearTimeout(timer);
    setTimer(setTimeout(handleFilter, 500));
  };

  const doScroll = value => {
    setScroller(value);
    const reffs = getVocReffs();
    for (let i = 0; i < wordStat.length; i++) {
      if (wordStat[i].WordEn.toLowerCase().startsWith(value.toLowerCase())) {
        if (lastElem) {
          lastElem.style["background-color"] = lastColor;
        }
        if (reffs[i] && reffs[i].current) {
          setLastColor(reffs[i].current.style["background-color"]);
          setLastElem(reffs[i].current);
          reffs[i].current.style["background-color"] = "yellow";
          reffs[i].current.scrollIntoView();
        }
        break;
      }
    }
  };

  const doOnBlur = () => {
    if (lastElem) {
      lastElem.style["background-color"] = lastColor;
      setLastElem(null);
    }
  };

  return (
    <div>
      <span>
        In user Vocabulary
        {' '}
        <span style={{ fontWeight: 600 }}>{wordStat.length}</span>
        {' '}
        words
      </span>
      <br />
      <br />
      <Input
        style={{ marginLeft: '-9px', border: 'solid 1px gray', borderRadius: '5px' }}
        icon="search"
        iconPosition="left"
        placeholder="Find Word"
        type="text"
        value={filter}
        onChange={event => {
          doSetFilter(event.target.value);
        }}
      />
      <br />
      <br />
      <Button color="blue" onClick={handleCancel} style={{ marginLeft: '138px', width: '85px' }}>
        Clear
      </Button>
      <br />
      <br />
      <br />
      <br />
      <Input
        style={{ marginLeft: '-9px', border: 'solid 1px gray', borderRadius: '5px' }}
        icon="film"
        iconPosition="left"
        placeholder="Scroll to Word"
        type="text"
        value={scroller}
        onSelect={() => doScroll(scroller)}
        onChange={event => {
          doScroll(event.target.value);
        }}
        onBlur={doOnBlur}
      />
      <br />
      <br />
      <br />
      <Checkbox
        toggle
        label="Show all user`s loggs"
        checked={showLoggs}
        onChange={toggleShowLoggs}
        style={{ marginLeft: '3px' }}
      />
    </div>
  );
};

VocabularyPanel.propTypes = {
  wordStat: PropTypes.arrayOf(PropTypes.object),
  showLoggs: PropTypes.bool
};

VocabularyPanel.defaultProps = {
  wordStat: [],
  showLoggs: false
};

const mapStateToProps = ({ profile }) => ({
  wordStat: profile.wordStat,
  showLoggs: profile.showLoggs
});

const actions = { setVocFilter, getVocReffs, toggleShowLoggs };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(VocabularyPanel);
