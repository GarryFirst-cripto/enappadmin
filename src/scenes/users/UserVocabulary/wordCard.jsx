import React, { useRef } from 'react';
import styles from '../styles.module.scss';

const WordCard = ({ word, filter, reference, setExtWord }) => {
  const { WordEn, WordRu, waight, state, count, countTrue, lastData } = word;

  const doShowLogs = () => {
    setExtWord(word);
  };

  const getWordName = () => {
    if (filter && filter.length > 0) {
      const i = WordEn.toLowerCase().indexOf(filter.toLowerCase());
      const j = filter.length;
      return (
        <span className={styles.nameSpan}>
          {' '}
          {WordEn.substr(0, i)}
          <span style={{ backgroundColor: '#8e8' }}>{WordEn.substr(i, j)}</span>
          {WordEn.substr(i + j)}
        </span>
      );
    };
    return (
      <span className={styles.nameSpan}>
        {' '}
        {WordEn}
      </span>
    );
  };

  const titleRef = useRef();

  return (
    <div ref={titleRef} className={styles.usercard} onDoubleClick={doShowLogs} onClick={doShowLogs}>
      {reference(titleRef)}
      <div style={{ display: 'flex', alignItems: 'center' }}>
        {getWordName()}
        <span className={styles.textWordSpan}>  
          {WordRu}
        </span>
        <span className={styles.textSpanRegg}>
          {' wg: '}
          {waight}            
        </span> 
        <span className={styles.textSpanCentre}>
          {state}            
        </span> 
        <span className={styles.textSpanCentre}>
          {count}
        </span>
        <span className={styles.textSpanCentre}>
          {count && countTrue
            ? ` er: ${count - countTrue}`
            : ''}
        </span> 
        <span className={styles.textSpanCentre}>
          {lastData 
            ? (new Date(lastData)).toLocaleString("ru-RU", {timeZone: "Europe/Moscow"}).substr(0, 5)
            : ''}
        </span> 
      </div>
    </div>
  );
};

export default WordCard;
