import React from 'react';
import styles from '../styles.module.scss';

const WordTitle = () => {
  return (
    <div className={styles.usercardTitle} >
      <div style={{ display: 'flex', alignItems: 'center' }}>
        <span className={styles.nameSpan}>
          {' '}
          {'English word'}
        </span>
        <span className={styles.textWordSpan}>  
          {'Translation'}
        </span>
        <span className={styles.textSpanRegg}>
          {'Word waight'}            
        </span> 
        <span className={styles.textSpanCentre}>
          {'Word State'}            
        </span> 
        <span className={styles.textSpanCentre}>
          {'Attempts'}
        </span>
        <span className={styles.textSpanCentre}>
          {'Errors'}
        </span> 
        <span className={styles.textSpanCentre}>
          {'Last Training'}
        </span> 
      </div>
    </div>
  );
};

export default WordTitle;
