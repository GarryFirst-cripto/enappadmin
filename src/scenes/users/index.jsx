import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Grid } from 'semantic-ui-react';
import UsersPage from './userPage';
import UsersPanel from './userPanel';
import VocabularyPanel from './UserVocabulary/userVocabularyPanel';

const Users = ({ expandedUser }) => {
  
  return (
    <div>
      <Grid columns="2" style={{ height: '92vh', width: '100%', marginLeft: '3px', marginTop: '3px' }}>
        <Grid.Column style={{ width: '250px', backgroundColor: '#eed' }}>
          {expandedUser
            ? <VocabularyPanel />
            : <UsersPanel />}
        </Grid.Column>
        <Grid.Column style={{ width: 'calc(100% - 250px)' }}>
          <UsersPage/>
        </Grid.Column>
      </Grid>
    </div>
  );
};

Users.propTypes = {
  expandedUser: PropTypes.objectOf(PropTypes.any)
};

Users.defaultProps = {
  expandedUser: undefined
};

const mapStateToProps = ({ users }) => ({
  expandedUser: users.expandedUser
});

export default connect(mapStateToProps)(Users);
