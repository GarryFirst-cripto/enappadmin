import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Loader } from 'semantic-ui-react';
import InfiniteScroll from 'react-infinite-scroller';
import { loadMoreUsers, editUser, updateUser, deleteUser, setReference, setStatLoading } from '../../services/actions';
import UserCard from './UserCard';
import UserInfoCard from './UserInfoCard';

const messageFilter = {
  from: 0,
  count: 10
};

const UsersPage = ({
  users,
  filter,
  hasMoreUsers,
  expandedUser,
  loadMoreUsers,
  editUser,
  setStatLoading,
  updateUser,
  deleteUser,
  setReference }) => {

  const getMoreUsers = () => {
    loadMoreUsers(messageFilter);
    const { from, count } = messageFilter;
    messageFilter.from = from + count;
  };

  return (
    expandedUser 
      ? <UserInfoCard user={expandedUser} editUser={editUser} updateUser={updateUser} deleteUser={deleteUser} />
      : <div>
        <InfiniteScroll
          pageStart={0}
          loadMore={getMoreUsers}
          hasMore={hasMoreUsers}
          style={{ overflowY: 'auto', height: 'calc(100vh - 105px)' }}
          loader={<Loader active inline="centered" key={0} />}>
          {users.map(user => (
            user.hidden ? null : <UserCard key={user.id} user={user} filter={filter} editUser={editUser} setStatLoading={setStatLoading} reference={setReference} />
          ))}
        </InfiniteScroll>
      </div>
  );
};

UsersPage.propTypes = {
  users: PropTypes.arrayOf(PropTypes.object),
  filter: PropTypes.string,
  hasMoreUsers: PropTypes.bool,
  expandedUser: PropTypes.objectOf(PropTypes.any),
  loadMoreUsers: PropTypes.func.isRequired,
  editUser: PropTypes.func.isRequired,
  updateUser: PropTypes.func.isRequired,
  deleteUser: PropTypes.func.isRequired,
  setReference: PropTypes.func.isRequired,
  setStatLoading: PropTypes.func.isRequired
};

UsersPage.defaultProps = {
  users: [],
  filter: '',
  hasMoreUsers: true,
  expandedUser: undefined
};

const mapStateToProps = ({ users }) => ({
  users: users.users,
  filter: users.filter,
  hasMoreUsers: users.hasMoreUsers,
  expandedUser: users.expandedUser
});

const actions = { loadMoreUsers, editUser, updateUser, deleteUser, setReference, setStatLoading };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(UsersPage);
