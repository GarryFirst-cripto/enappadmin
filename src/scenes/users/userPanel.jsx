import React, { useState } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Input, Button } from 'semantic-ui-react';
import { setUserFilter, getReffs } from '../../services/actions';
// import styles from './styles.module.scss';

const UsersPanel = ({ users, setUserFilter, getReffs }) => {
  
  const [filter, setFilter] = useState('');
  const [scroller, setScroller] = useState('');
  const [timer, setTimer] = useState();
  const [lastElem, setLastElem] = useState();
  const [lastColor, setLastColor] = useState();
  let filtText;

  const handleFilter = () => {
    clearTimeout(timer);
    setUserFilter(filtText);
  };

  const handleCancel = () => {
    clearTimeout(timer);
    setFilter('');
    setUserFilter('');
  };

  const doSetFilter = value => {
    setFilter(value);
    filtText = value;
    clearTimeout(timer);
    setTimer(setTimeout(handleFilter, 500));
  };

  const doScroll = value => {
    setScroller(value);
    const reffs = getReffs();
    for (let i = 0; i < users.length; i++) {
      if (users[i].email.toLowerCase().startsWith(value.toLowerCase())) {
        if (lastElem) {
          lastElem.style["background-color"] = lastColor;
        }
        setLastColor(reffs[i].current.style["background-color"]);
        setLastElem(reffs[i].current);
        reffs[i].current.style["background-color"] = "yellow";
        reffs[i].current.scrollIntoView();
        break;
      }
    }
  };

  const doOnBlur = () => {
    if (lastElem) {
      lastElem.style["background-color"] = lastColor;
      setLastElem(null);
    }
  };

  return (
    <div>
      <span>
        Registered
        {' '}
        <span style={{ fontWeight: 600 }}>{users.length}</span>
        {' '}
        users
      </span>
      <br />
      <br />
      <Input
        style={{ marginLeft: '-9px', border: 'solid 1px gray', borderRadius: '5px' }}
        icon="search"
        iconPosition="left"
        placeholder="Find User by name"
        type="text"
        value={filter}
        onChange={event => {
          doSetFilter(event.target.value);
        }}
      />
      <br />
      <br />
      <Button color="blue" onClick={handleCancel} style={{ marginLeft: '138px', width: '85px' }}>
        Clear
      </Button>
      <br />
      <br />
      <br />
      <br />
      <Input
        style={{ marginLeft: '-9px', border: 'solid 1px gray', borderRadius: '5px' }}
        icon="film"
        iconPosition="left"
        placeholder="Scroll to User"
        type="text"
        value={scroller}
        onSelect={() => {
          doScroll(scroller);
        }}
        onChange={event => {
          doScroll(event.target.value);
        }}
        onBlur={doOnBlur}
      />
    </div>
  );
};

UsersPanel.propTypes = {
  users: PropTypes.arrayOf(PropTypes.object)
};

UsersPanel.defaultProps = {
  users: []
};

const mapStateToProps = ({ users }) => ({
  users: users.users
});

const actions = { setUserFilter, getReffs };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(UsersPanel);
