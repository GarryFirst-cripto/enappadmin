import { history } from 'src/store/store';
import * as authService from 'src/services/authService';
import * as usersService from 'src/services/usersService';
import * as wordService from 'src/services/wordsService';
import { SET_USER } from '../store/profileReducer';
import { LOAD_MORE_USERS, EDIT_USER, UPDATE_USERS_LIST, SET_USERS_FILTER } from '../store/usersReducer';
import { RESET_REFERENCES, SET_REFERENCE, RESET_VOC_REFERENCES, SET_VOC_REFERENCE } from '../store/reffReducer';
import { SET_STATISTIC, SET_STAT_LOADING, SET_USER_PAGE, SET_WORDS_FILTER, UPDATE_WORDS_LIST, SET_EXT_WORD, SET_WORDS_LOGGS, SET_SHOW_LOGGS } from '../store/profileReducer';

const setToken = token => localStorage.setItem('token', token);

const setUser = user => async dispatch => dispatch({
  type: SET_USER,
  user
});

const addMoreUsersAction = users => ({
  type: LOAD_MORE_USERS,
  users
});

const updateUsersList = users => ({
  type: UPDATE_USERS_LIST,
  users
});

const editUserAction = user => ({
  type: EDIT_USER,
  user
});

const setUserFilterAction = filter => ({
  type: SET_USERS_FILTER,
  filter
});

const updateWordsList = wordStat => ({
  type: UPDATE_WORDS_LIST,
  wordStat
});

const setWordsFilterAction = filter => ({
  type: SET_WORDS_FILTER,
  filter
});

const resetReffsAction = () => ({
  type: RESET_REFERENCES
});

const setReffAction = reference => ({
  type: SET_REFERENCE,
  reference
});

const resetVocReffsAction = () => ({
  type: RESET_VOC_REFERENCES
});

const setVocReffAction = reference => ({
  type: SET_VOC_REFERENCE,
  reference
});

const setStatLoadingAction = () => ({
  type: SET_STAT_LOADING
});

const setUserStatistic = (stat, wordStat) => ({
  type: SET_STATISTIC,
  stat,
  wordStat  
});

const setUserPagePage = isPage => ({
  type: SET_USER_PAGE,
  isPage
});

const setExtWordAction = extWord => ({
  type: SET_EXT_WORD,
  extWord
});

const setWordLoggAction = loggs => ({
  type: SET_WORDS_LOGGS,
  loggs
});

const showLoggsAction = showLoggs => ({
  type: SET_SHOW_LOGGS,
  showLoggs
});

const handleAuthResponse = authResponsePromise => async (dispatch, getRootState) => {
  const { user, token } = await authResponsePromise;
  setToken(token);
  setUser(user)(dispatch, getRootState);
  return user;
};

export const login = request => handleAuthResponse(authService.login(request));

export const logout = () => {
  localStorage.clear();
  window.location.href = '/';
};

export const doAdminLogin = token => async (dispatch, getRootState) => {
  const data = await authService.adminLogin(token);
  if (data) {
    setUser(data)(dispatch, getRootState);
  } else {
    setUser(null)(dispatch, getRootState);
  }
};

export const doAdminEntrance = () => async (dispatch, getRootState) => {
  setUser(null)(dispatch, getRootState);
  history.push('/login');
};

export const loadMoreUsers = filter => async (dispatch, getRootState) => {
  const { users: { users, filter } } = getRootState();
  const loadedUsers = await usersService.getAllUsers(filter);
  const filteredUsers = loadedUsers.filter(user => !(users && users.some(loadedUsers => user.id === loadedUsers.id)));
  if (filteredUsers) {
    if (filter) {
      filteredUsers.forEach(item => {
        item.hidden = (item.email.indexOf(filter) < 0);
      });
    }
    dispatch(addMoreUsersAction(filteredUsers));
  };
};

export const setStatLoading = () => dispatch => {
  dispatch(setStatLoadingAction());
};

export const getStatistic = () => async (dispatch, getRootState) => {
  const { users: { expandedUser }} = getRootState();
  const userId = expandedUser.id;
  const { stat, wordStat } = await usersService.getStatistic({ userId });
  dispatch(setUserStatistic(stat, wordStat));
};

export const setUserPage = isPage => dispatch => {
  dispatch(setUserPagePage(isPage));
};

export const editUser = user => async dispatch => {
  dispatch(editUserAction(user));
};

export const updateUser = user => async (dispatch, getRootState) => {
  const updUser = await usersService.getUpdateUser(user);
  const { id } = updUser;
  if (id) {
    const { users: { users } } = getRootState();
    const updated = users.map(item => (item.id !== id ? item : updUser));
    dispatch(updateUsersList(updated));
  }
};

export const deleteUser = id => async (dispatch, getRootState) => {
  const { result } = await usersService.getDeleteUser(id);
  if (result > 0) {
    const { users: { users } } = getRootState();
    const updated = [];
    users.forEach(item => {
      if (item.id !== id) updated.push(item);
    });
    dispatch(updateUsersList(updated));
  }
};

export const setUserFilter = filter => (dispatch, getRootState) => {
  const { users: { users } } = getRootState();
  const filteredUsers = filter 
    ?  users.map(item => {
      const hidden = (item.email.toLowerCase().indexOf(filter.toLowerCase()) < 0);
      return { ...item, hidden };
    })
    : users.map(item => {
      return { ...item, hidden: false };
    });
  dispatch(setUserFilterAction(filter));
  dispatch(updateUsersList(filteredUsers));
};

export const setVocFilter = filter => (dispatch, getRootState) => {
  const { profile: { wordStat } } = getRootState();
  const filteredWords = filter
    ? wordStat.map(item => {
      const hidden = (item.WordEn.toLowerCase().indexOf(filter.toLowerCase()) < 0);
      return { ...item, hidden };
    })
    : wordStat.map(item => {
      return { ...item, hidden: false };
    });
  dispatch(setWordsFilterAction(filter));
  dispatch(updateWordsList(filteredWords));
};

export const setReference = reference => dispatch => {
  if (reference) {
    dispatch(setReffAction(reference));
  } else {
    dispatch(resetReffsAction());
  }
};

export const getReffs = () => (_dispatch, getRootState) => {
  const { reffs: { reff } } = getRootState();
  return reff;
};

export const setVocReference = reference => dispatch => {
  if (reference) {
    dispatch(setVocReffAction(reference));
  } else {
    dispatch(resetVocReffsAction());
  }
};

export const getVocReffs = () => (_dispatch, getRootState) => {
  const { reffs: { vocreff } } = getRootState();
  return vocreff;
};

export const toggleShowLoggs = () => (dispatch, getRootState) => {
  const { profile: { showLoggs }} = getRootState();
  dispatch(showLoggsAction(!showLoggs));
};

export const setExtWord = extWord => async (dispatch, getRootState) => {
  dispatch(setExtWordAction(extWord));
  const { users: { expandedUser } } = getRootState();
  const userId = expandedUser.id;
  const { profile: { showLoggs } } = getRootState();
  const { wordEnId, wordRuId } = extWord;
  const list = showLoggs 
    ? await wordService.getWordLoggAction({ wordEnId, wordRuId })
    : await wordService.getWordLoggAction({ wordEnId, wordRuId, userId });
  if (list) {
    const loggs = list.sort((itemA, itemB) => {
      const time = (new Date(itemA.createdAt)).getTime() - (new Date(itemB.createdAt)).getTime();
      if (time === 0) return (itemA.index - itemB.index);
      return time;
    });
    dispatch(setWordLoggAction(loggs));
  } else {
    dispatch(setWordLoggAction(list));
  }
};
