import callWebApi from 'src/helpers/apiHelper';

export const login = async query => {
  try {
    const response = await callWebApi({
      endpoint: '/api/users/admin/auth',
      type: 'GET',
      query
    });
    return response.json();
  } catch (e) {
    return { user: null, token: null };
  }
};

export const adminLogin = async token => {
  try {
    const response = await callWebApi({
      endpoint: '/api/users/user',
      type: 'GET'
    });
    return response.json();
  } catch (e) {
    return null;
  }
};
