import callWebApi from 'src/helpers/apiHelper';

export const getAllUsers = async query => {
  try {
    const response = await callWebApi({
      endpoint: '/api/users',
      type: 'GET',
      query
    });
    return response.json();
  } catch (e) {
    return [];
  }
};

export const getUpdateUser = async request => {
  try {
    const response = await callWebApi({
      endpoint: '/api/users',
      type: 'PUT',
      request
    });
    return response.json();
  } catch (e) {
    return [];
  }
};

export const getDeleteUser = async id => {
  try {
    const response = await callWebApi({
      endpoint: `/api/users/${id}`,
      type: 'DELETE'
    });
    return response.json();
  } catch (e) {
    return [];
  }
};

export const getStatistic = async query => {
  try {
    const response = await callWebApi({
      endpoint: '/api/data/statistic',
      type: 'GET',
      query
    });
    return response.json();
  } catch (e) {
    return [];
  }
};
