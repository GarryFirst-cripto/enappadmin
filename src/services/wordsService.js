import callWebApi from 'src/helpers/apiHelper';

export const writeWordsAction = async wordList => {
  try {
    const response = await callWebApi({
      endpoint: '/api/words/words',
      type: 'POST',
      request: wordList
    });
    return response.json();
  } catch (e) {
    return null;
  }
};

export const getWordLoggAction = async query => {
  try {
    const response = await callWebApi({
      endpoint: '/api/logs/full',
      type: 'GET',
      query
    });
    return response.json();
  } catch (e) {
    return null;
  }
};
