
export const SET_USER = 'PROFILE_ACTION:SET_USER';
export const SET_STAT_LOADING = 'PROFILE_STAT_LOADING';
export const SET_STATISTIC = 'PROFILE_SET_STATISTIC';
export const SET_USER_PAGE = 'PROFILE_SET_USERPAGE';
export const SET_WORDS_FILTER = 'PROFILE_SET_FILTER';
export const SET_WORDS_LOGGS = 'PROFILE_SET_WORD_LOGG';
export const SET_EXT_WORD = 'PROFILE_SET_EXT_WORD';
export const UPDATE_WORDS_LIST = 'PROFILE_UPDATE_WORDS';
export const SET_SHOW_LOGGS = 'PROFILE_SET_SHOW_LOGGS';

const reducer = (state = {}, action) => {
  switch (action.type) {
    case SET_USER:
      return {
        ...state,
        user: action.user,
        isAuthorized: Boolean(action.user?.id),
        isLoading: false
      };
    case SET_STAT_LOADING:
      return {
        ...state,
        stat: [],
        wordStat: [],
        isStatLoading: true,
        extWord: undefined
      };
    case SET_STATISTIC:
      return {
        ...state,
        stat: action.stat,
        wordStat: action.wordStat,
        isStatLoading: false
      };
    case SET_USER_PAGE:
      return {
        ...state,
        isPage: action.isPage
      };
    case UPDATE_WORDS_LIST:
      return {
        ...state,
        wordStat: action.wordStat
      };
    case SET_WORDS_FILTER:
      return {
        ...state,
        filter: action.filter
      };
    case SET_EXT_WORD:
      return {
        ...state,
        extWord: action.extWord,
        isLoggLoading: true
      };
    case SET_WORDS_LOGGS:
      return {
        ...state,
        loggs: action.loggs,
        isLoggLoading: false
      };
    case SET_SHOW_LOGGS:
      return {
        ...state,
        showLoggs: action.showLoggs
      };
    default:
      return state;
  }
};

export default reducer;
