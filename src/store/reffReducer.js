export const RESET_REFERENCES = 'RESET_ALL_REFF';
export const SET_REFERENCE = 'SET_NEW_REFF';
export const RESET_VOC_REFERENCES = 'RESET_ALL_VOCS_REFF';
export const SET_VOC_REFERENCE = 'SET_NEW_VOCS_REFF';

const reffReducer = (state = { reff: [], vocreff: [] }, action) => {
  switch (action.type) {
    case RESET_REFERENCES:
      return { ...state, reff: [] };
    case SET_REFERENCE:
      state.reff.push(action.reference);
      return state;
    case RESET_VOC_REFERENCES:
      return { ...state, vocreff: [] };
    case SET_VOC_REFERENCE:
      state.vocreff.push(action.reference);
      return state;
    default:
      return state;
  }
};

export default reffReducer;
