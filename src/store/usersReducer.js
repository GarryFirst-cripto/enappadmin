export const LOAD_MORE_USERS = 'LOAD_MORE_USERS';
export const EDIT_USER = 'DO_EDIT_USER';
export const UPDATE_USERS_LIST = 'DO_UPDATE_USES';
export const SET_USERS_FILTER = 'DO_SET_USERS_FILTER';

const userReducer = (state = {}, action) => {
  switch (action.type) {
    case LOAD_MORE_USERS:
      return {
        ...state,
        users: [...(state.users || []), ...action.users],
        hasMoreUsers: Boolean(action.users.length)
      };
    case EDIT_USER:
      return {
        ...state,
        expandedUser: action.user
      };
    case UPDATE_USERS_LIST:
      return {
        ...state,
        users: action.users
      };
    case SET_USERS_FILTER:
      return {
        ...state,
        filter: action.filter
      };
    default:
      return state;
  }
};

export default userReducer;
